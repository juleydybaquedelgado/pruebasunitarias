package com.example.pruebaunitaria;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class Main extends Fragment {

    private static final String ARG_PARAMETRO1 = "parametro1";
    private static final String ARG_PARAMETRO2 = "parametro2";

    private String param1;
    private String param2;

    public Main(){
    }

    public static Main newInstance(String parametro1, String parametro2){
        Main fragment = new Main();
        Bundle args = new Bundle();
        args.putString(ARG_PARAMETRO1, parametro1);
        args.putString(ARG_PARAMETRO2, parametro2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstancesState){
        super.onCreate(savedInstancesState);
        if (getArguments()!=null){
            param1 = getArguments().getString(ARG_PARAMETRO1);
            param2 = getArguments().getString(ARG_PARAMETRO2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.pruebas, container, false);
    }
}
